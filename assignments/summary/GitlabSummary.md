                                                        **Gitlab summary**



- Gitlab is a service that provides remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle. 
- These additional features include managing the sharing of code between different people, bug tracking, wiki space and other tools for 'social coding' and GitLab is a github like service that organizations can use to provide internal management of git repositories. It is a self hosted Git-repository management system that keeps the user code private and can easily deploy the changes of the code.

- For installation refer [this][x].

[x]: https://www.tutorialspoint.com/gitlab/gitlab_installation.htm