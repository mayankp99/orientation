                                                     Git Summary
Git is a Version Control System (VCS) is a software that helps software developers to work together and maintain a complete history of their work.


There are four fundamental elements in the Git Workflow-

1. Working Directory.
2. Staging Area. 
3. Local Repository. 
4. Remote Repository.

Here are some important definitions -

**Repository**:
Repositories in GIT contain a collection of files of various different versions of a Project. These files are imported from the repository into the local server of the user for further updations and modifications in the content of the file.

**GitLab**:
The 2nd most popular remote storage solution for git repos.

**Commit**:
Commit holds the current state of the repository. A commit is also named by SHA1 hash. You can consider a commit object as a node of the linked list.

**Branch**:
A branch is nothing but a pointer to the latest commit in the Git repository. 

**Merge**:
Merging is Git's way of putting a forked history back together again. The git merge command lets you take the independent lines of development created by git branch and integrate them into a single branch.

**Clone**:
Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.

**Fork**:
Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

**Push**:
Push operation copies changes from a local repository instance to a remote one. This is used to store the changes permanently into the Git repository. This is same as the commit operation in Subversion.

**Pull**:
Pull operation copies the changes from a remote repository instance to a local one. The pull operation is used for synchronization between two repository instances. This is same as the update operation in Subversion.